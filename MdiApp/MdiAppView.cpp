﻿// 这段 MFC 示例源代码演示如何使用 MFC Microsoft Office Fluent 用户界面
// (“Fluent UI”)。该示例仅供参考，
// 用以补充《Microsoft 基础类参考》和
// MFC C++ 库软件随附的相关电子文档。
// 复制、使用或分发 Fluent UI 的许可条款是单独提供的。
// 若要了解有关 Fluent UI 许可计划的详细信息，请访问
// https://go.microsoft.com/fwlink/?LinkId=238214.
//
// 版权所有(C) Microsoft Corporation
// 保留所有权利。

// MdiAppView.cpp: CMdiAppView 类的实现
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "MdiApp.h"
#endif

#include "MdiAppDoc.h"
#include "MdiAppView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMdiAppView

IMPLEMENT_DYNCREATE(CMdiAppView, CView)

BEGIN_MESSAGE_MAP(CMdiAppView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CMdiAppView::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// CMdiAppView 构造/析构

CMdiAppView::CMdiAppView() noexcept
{
	// TODO: 在此处添加构造代码

}

CMdiAppView::~CMdiAppView()
{
}

BOOL CMdiAppView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CMdiAppView 绘图

void CMdiAppView::OnDraw(CDC* /*pDC*/)
{
	CMdiAppDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)	return;
		

	// TODO: 在此处为本机数据添加绘制代码
}


// CMdiAppView 打印


void CMdiAppView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CMdiAppView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CMdiAppView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CMdiAppView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}

void CMdiAppView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CMdiAppView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CMdiAppView 诊断

#ifdef _DEBUG
void CMdiAppView::AssertValid() const
{
	CView::AssertValid();
}

void CMdiAppView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMdiAppDoc* CMdiAppView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMdiAppDoc)));
	return (CMdiAppDoc*)m_pDocument;
}
#endif //_DEBUG


// CMdiAppView 消息处理程序


BOOL CMdiAppView::DestroyWindow()
{
    // TODO: 在此添加专用代码和/或调用基类
  
    return CView::DestroyWindow();
}
