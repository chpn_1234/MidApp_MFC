#include "pch.h"

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "MdiApp.h"
#endif

#include "MyDocument1.h"
#include "MyDocument1View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMdiAppView

IMPLEMENT_DYNCREATE(MyDocument1View, CView)

BEGIN_MESSAGE_MAP(MyDocument1View, CView)
    // 标准打印命令
    ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
    ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
    ON_COMMAND(ID_FILE_PRINT_PREVIEW, &MyDocument1View::OnFilePrintPreview)
    ON_WM_CONTEXTMENU()
    ON_WM_RBUTTONUP()
    ON_WM_LBUTTONDOWN()
    ON_WM_LBUTTONUP()
    ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()



// CMdiAppView 构造/析构

MyDocument1View::MyDocument1View() noexcept
{
    // TODO: 在此处添加构造代码

}

MyDocument1View::~MyDocument1View()
{
}

BOOL MyDocument1View::PreCreateWindow(CREATESTRUCT &cs)
{
    // TODO: 在此处通过修改
    //  CREATESTRUCT cs 来修改窗口类或样式

    return CView::PreCreateWindow(cs);
}

// CMdiAppView 绘图

void MyDocument1View::OnDraw(CDC * /*pDC*/)
{
    MyDocument1 *pDoc = GetDocument();
    ASSERT_VALID(pDoc);
    if ( !pDoc ) return;



    /**
    * step 1:draw created rectangles  
    */
    {
        CClientDC dc(this);
        CBrush *pBrush		= CBrush::FromHandle((HBRUSH)GetStockObject(NULL_BRUSH));
        CBrush *pOldBrush	= dc.SelectObject(pBrush);
        CPen pen1(PS_SOLID, 2, RGB(0, 255, 0));
        CPen *pOldPen = dc.SelectObject(&pen1);


        CTypedPtrList <CPtrList, CRetangle *> &tempList = this->GetDocument()->m_typedPtrList;//+++++++得到CTestDoc中的成员
        POSITION ListPosition = tempList.GetHeadPosition();//+++++得到头的位置
        while ( ListPosition != NULL )
        {
            CRetangle *pCPRetangle = tempList.GetNext(ListPosition);//++++++画图
            dc.Rectangle(CRect(pCPRetangle->PointX, pCPRetangle->PointY));
        }

        dc.SelectObject(pOldPen);
        dc.SelectObject(pOldBrush);
    }


   /*
   * step 2:draw temp rectangle
   */
    {
        CClientDC dc(this);
        CBrush *pBrush		= CBrush::FromHandle((HBRUSH)GetStockObject(NULL_BRUSH));
        CBrush *pOldBrush	= dc.SelectObject(pBrush);
        CPen pen1(PS_SOLID, 5, RGB(0, 255, 255));
        CPen *pOldPen = dc.SelectObject(&pen1);
        if ( m_LButtonDown == true )
        {
            dc.Rectangle(CRect(m_PointStart, m_PointEnd));
        }

        dc.SelectObject(pOldPen);
        dc.SelectObject(pOldBrush);
    }


}


// CMdiAppView 打印


void MyDocument1View::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
    AFXPrintPreview(this);
#endif
}

BOOL MyDocument1View::OnPreparePrinting(CPrintInfo *pInfo)
{
    // 默认准备
    return DoPreparePrinting(pInfo);
}

void MyDocument1View::OnBeginPrinting(CDC * /*pDC*/, CPrintInfo * /*pInfo*/)
{
    // TODO: 添加额外的打印前进行的初始化过程
}

void MyDocument1View::OnEndPrinting(CDC * /*pDC*/, CPrintInfo * /*pInfo*/)
{
    // TODO: 添加打印后进行的清理过程
}


void MyDocument1View::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
    ClientToScreen(&point);
    OnContextMenu(this, point);
}


void MyDocument1View::OnContextMenu(CWnd * /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
    theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}




// MyDocument1View 诊断

#ifdef _DEBUG
void MyDocument1View::AssertValid() const
{
    CView::AssertValid();
}



void MyDocument1View::Dump(CDumpContext &dc) const
{
    CView::Dump(dc);
}



MyDocument1 *MyDocument1View::GetDocument() const // 非调试版本是内联的
{
    ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(MyDocument1)));
    return (MyDocument1 *)m_pDocument;
}
#endif //_DEBUG



void MyDocument1View::OnLButtonDown(UINT nFlags, CPoint point)
{
    m_LButtonDown = true;
    m_PointStart = point;

    CView::OnLButtonDown(nFlags, point);
}



void MyDocument1View::OnLButtonUp(UINT nFlags, CPoint point)
{
    m_LButtonDown = false;
    m_PointEnd = point;
    CRetangle *pCPRetangle = new CRetangle(m_PointStart, m_PointEnd);
    this->GetDocument()->m_typedPtrList.AddTail(pCPRetangle);
    Invalidate(true);						//重绘	擦除背景
    this->GetDocument()->SetModifiedFlag(true);
    CView::OnLButtonUp(nFlags, point);
}



void MyDocument1View::OnMouseMove(UINT nFlags, CPoint point)
{
    if ( m_LButtonDown == false ) return;

    m_PointEnd = point;
    Invalidate(true);		//重绘	擦除背景

    CView::OnMouseMove(nFlags, point);
}
