﻿#pragma once
#include "afxdialogex.h"


// DlgMyDocumentTypeSet 对话框

class DlgMyDocumentTypeSet : public CDialogEx
{
	DECLARE_DYNAMIC(DlgMyDocumentTypeSet)

public:
	DlgMyDocumentTypeSet(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgMyDocumentTypeSet();

	// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_MY_DOCUMENT_TYPE_SET };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	void		 InitCombDocumentType();

	DECLARE_MESSAGE_MAP()
    

	EFileDocumentType			m_nDocumentType;
	CComboBox	                m_crtlCombDocumentType;
public:
	EFileDocumentType		GetDocumentType() { return  m_nDocumentType; }
	void		SetDocumentType(EFileDocumentType nType) {m_nDocumentType = nType;}
    afx_msg void OnBnClickedOk();
    afx_msg void OnPaint();
    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
