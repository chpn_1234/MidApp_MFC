﻿/*******************************************************************************
 * @file       FileView.h
 * @brief
 * @details
 * @author
 * @date       2023-02-10
 * @version    V1.0
 * @copyright  Copyright (c) 2022-2025  Poisson Software Technology LTD
 *******************************************************************************/

#pragma once

#include "ViewTree.h"


class CFileViewToolBar: public CMFCToolBar
{
    virtual void OnUpdateCmdUI(CFrameWnd * /*pTarget*/, BOOL bDisableIfNoHndler)
    {
        CMFCToolBar::OnUpdateCmdUI((CFrameWnd *)GetOwner(), bDisableIfNoHndler);
    }

    virtual BOOL AllowShowOnList() const
    {
        return FALSE;
    }
};


class CFileView: public CDockablePane
{
    // 构造
public:
    CFileView() noexcept;
    virtual ~CFileView();

    void        AdjustLayout();
    void        OnChangeVisualStyle();

    CViewTree   *getMyViewTree();
    void        activeSelectedDocumentTab();


private:
    void fillViewTree();


    // 特性
protected:
    CViewTree           m_wndFileView;
    CImageList          m_FileViewImages;
    CFileViewToolBar    m_wndToolBar;


   

  
    afx_msg int     OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void    OnSize(UINT nType, int cx, int cy);
    afx_msg void    OnContextMenu(CWnd *pWnd, CPoint point);
    afx_msg void    OnProperties();
    afx_msg void    OnFileOpen();
    afx_msg void    OnFileOpenWith();
    afx_msg void    OnDummyCompile();
    afx_msg void    OnEditCut();
    afx_msg void    OnEditCopy();
    afx_msg void    OnEditClear();
    afx_msg void    OnPaint();
    afx_msg void    OnSetFocus(CWnd *pOldWnd);
    afx_msg void    OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
    DECLARE_MESSAGE_MAP()

};