﻿// 这段 MFC 示例源代码演示如何使用 MFC Microsoft Office Fluent 用户界面
// (“Fluent UI”)。该示例仅供参考，
// 用以补充《Microsoft 基础类参考》和
// MFC C++ 库软件随附的相关电子文档。
// 复制、使用或分发 Fluent UI 的许可条款是单独提供的。
// 若要了解有关 Fluent UI 许可计划的详细信息，请访问
// https://go.microsoft.com/fwlink/?LinkId=238214.
//
// 版权所有(C) Microsoft Corporation
// 保留所有权利。

#include "pch.h"
#include "framework.h"
#include "ViewTree.h"
#include "MdiApp.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CViewTree

CViewTree::CViewTree() noexcept
{
}

CViewTree::~CViewTree()
{
}

BEGIN_MESSAGE_MAP(CViewTree, CTreeCtrl)
    ON_WM_LBUTTONDBLCLK()
    ON_WM_LBUTTONUP()
    ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CViewTree 消息处理程序

BOOL CViewTree::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT *pResult)
{
    BOOL bRes = CTreeCtrl::OnNotify(wParam, lParam, pResult);

    NMHDR *pNMHDR = (NMHDR *)lParam;
    ASSERT(pNMHDR != nullptr);

#pragma warning(suppress : 26454)
    if(pNMHDR && pNMHDR->code == TTN_SHOW && GetToolTips() != nullptr)
    {
        GetToolTips()->SetWindowPos(&wndTop, -1, -1, -1, -1, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOSIZE);
    }

    return bRes;
}



void CViewTree::OnLButtonDblClk(UINT nFlags, CPoint point)
{

    CTreeCtrl::OnLButtonDblClk(nFlags, point);

    HTREEITEM   hCurSelectedItem    = GetSelectedItem();
    if(GetNextSiblingItem(hCurSelectedItem) == NULL)
    {
        return;
    }

    CString     strSelectedItenText = GetItemText(hCurSelectedItem);
    AfxGetApp()->OpenDocumentFile(strSelectedItenText);
}



void CViewTree::InitViewTree()
{
    SetRedraw(FALSE);
    DeleteAllItems();
    m_mapDocumentItemPathName.erase(m_mapDocumentItemPathName.begin(), m_mapDocumentItemPathName.end());
    SetRedraw(TRUE);


    HTREEITEM hRoot = InsertItem(_T("文件"), 0, 0);
    SetItemState(hRoot, TVIS_BOLD, TVIS_BOLD);


    CDocManager *appDocumentManager = ((MdiAppApp *)AfxGetApp())->getAppDocumentManager();
    if(!appDocumentManager)
    {
        return;
    }


    POSITION    docTemplatePosition                 = appDocumentManager->GetFirstDocTemplatePosition();
    POSITION    docPosition                         = NULL;
    CDocTemplate *pDocTemplate                      = nullptr;
    CDocument *pDoc                                 = nullptr;
    int         nDocTemplateNum                     = 0;
    int         nDocNum                             = 0;
    CString     strDocTemplateName                  = _T("文件类型");
    CString     strDocTitle                         = _T("文件");
    CString     strDocPathName                      = _T("文件");
    HTREEITEM   hDocTemplateItem                    = InsertItem(strDocTemplateName, 0, 0, hRoot);
    HTREEITEM   hDocItem                            = NULL;
    size_t      nDocItemIndex                       = 0;

    while(docTemplatePosition != NULL)
    {
        pDocTemplate =  appDocumentManager->GetNextDocTemplate(docTemplatePosition);
        pDocTemplate->GetDocString(strDocTemplateName, CDocTemplate::docName);

        hDocTemplateItem  = InsertItem(strDocTemplateName, 0, 0, hRoot);

        docPosition = pDocTemplate->GetFirstDocPosition();
        while(docPosition != NULL)
        {
            pDoc            =  pDocTemplate->GetNextDoc(docPosition);
            strDocTitle     = pDoc->GetTitle();
            strDocPathName  = pDoc->GetPathName();


            //1 set item text by pathname
            /* if (strDocPathName.GetLength() >= strDocTitle.GetLength())
             {
                 hDocItem        = InsertItem(strDocPathName, 1, 1, hDocTemplateItem);
             }
             else
             {
                 hDocItem        = InsertItem(strDocTitle, 1, 1, hDocTemplateItem);
             }*/


             //2 set item text by title, save item  pathname
            hDocItem        = InsertItem(strDocTitle, 1, 1, hDocTemplateItem);
            nDocItemIndex   = m_mapDocumentItemPathName.size();
            SetItemData(hDocItem, nDocItemIndex);
            m_mapDocumentItemPathName.insert(std::pair<HTREEITEM, CString>(hDocItem, strDocPathName));
        }


        Expand(hDocTemplateItem, TVE_EXPAND);
    }

    Expand(hRoot, TVE_EXPAND);
}



CString CViewTree::GetDocumentItemPathName(HTREEITEM item)
{
    CString strPathName = _T("");
    std::map<HTREEITEM, CString>::iterator itor  =  m_mapDocumentItemPathName.find(item);

    if(itor != m_mapDocumentItemPathName.end())
    {
        strPathName = itor->second;
    }
    return strPathName;
}


void CViewTree::OnLButtonUp(UINT nFlags, CPoint point)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值

    CTreeCtrl::OnLButtonUp(nFlags, point);


}


void CViewTree::OnLButtonDown(UINT nFlags, CPoint point)
{
    HTREEITEM  selecteItem = GetSelectedItem();
    CTreeCtrl::OnLButtonDown(nFlags, point);

  


    BEGIN_GET_LOCAL_TIME();
    END_GET_LOCAL_TIME(123);

    //ScreenToClient(&point);//转化为客户坐标
    HTREEITEM hitItem = HitTest(point, &nFlags);

    if((nFlags & TVHT_NOWHERE) ||
       (nFlags & TVHT_ONITEMRIGHT)||
       (nFlags & TVHT_ONITEMINDENT))
    {
        MessageBox(_T("取消选中"));
        SelectItem(NULL);
        PostMessage(WM_KILLFOCUS, 0, 0);
    }


    if((nFlags & TVHT_ONITEMLABEL) 
    && selecteItem ==hitItem)
    { 
        MessageBox(_T("selested too many"));
    }
  
}
