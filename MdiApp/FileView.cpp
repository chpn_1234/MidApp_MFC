﻿// 这段 MFC 示例源代码演示如何使用 MFC Microsoft Office Fluent 用户界面 
// (“Fluent UI”)。该示例仅供参考，
// 用以补充《Microsoft 基础类参考》和 
// MFC C++ 库软件随附的相关电子文档。  
// 复制、使用或分发 Fluent UI 的许可条款是单独提供的。  
// 若要了解有关 Fluent UI 许可计划的详细信息，请访问 
// https://go.microsoft.com/fwlink/?LinkId=238214.
//
// 版权所有(C) Microsoft Corporation
// 保留所有权利。

#include "pch.h"
#include "framework.h"
#include "mainfrm.h"
#include "ChildFrm.h"
#include "FileView.h"
#include "Resource.h"
#include "MdiApp.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileView

CFileView::CFileView() noexcept
{
}

CFileView::~CFileView()
{
}

BEGIN_MESSAGE_MAP(CFileView, CDockablePane)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_CONTEXTMENU()
    ON_COMMAND(ID_PROPERTIES, &CFileView::OnProperties)
    ON_COMMAND(ID_OPEN, &CFileView::OnFileOpen)
    ON_COMMAND(ID_OPEN_WITH, &CFileView::OnFileOpenWith)
    ON_COMMAND(ID_DUMMY_COMPILE, &CFileView::OnDummyCompile)
    ON_COMMAND(ID_EDIT_CUT, &CFileView::OnEditCut)
    ON_COMMAND(ID_EDIT_COPY, &CFileView::OnEditCopy)
    ON_COMMAND(ID_EDIT_CLEAR, &CFileView::OnEditClear)
    ON_WM_PAINT()
    ON_WM_SETFOCUS()
    ON_WM_SETTINGCHANGE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWorkspaceBar 消息处理程序

int CFileView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDockablePane::OnCreate(lpCreateStruct) == -1)
        return -1;

    CRect rectDummy;
    rectDummy.SetRectEmpty();

    // 创建视图: 
    const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS;

    if (!m_wndFileView.Create(dwViewStyle, rectDummy, this, 4))
    {
        TRACE0("未能创建文件视图\n");
        return -1;      // 未能创建
    }


    // Load File View Images
    m_FileViewImages.Create(IDB_FILE_VIEW, 16, 0, RGB(255, 0, 255));
    m_wndFileView.SetImageList(&m_FileViewImages, TVSIL_NORMAL);


    m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_EXPLORER);
    m_wndToolBar.LoadToolBar(IDR_EXPLORER, 0, 0, TRUE /* 已锁定*/);

    OnChangeVisualStyle();

    m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);

    m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));

    m_wndToolBar.SetOwner(this);

    // 所有命令将通过此控件路由，而不是通过主框架路由: 
    m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

    // 填入一些静态树视图数据(此处只需填入虚拟代码，而不是复杂的数据)
    fillViewTree();
    AdjustLayout();

    return 0;
}

void CFileView::OnSize(UINT nType, int cx, int cy)
{
    CDockablePane::OnSize(nType, cx, cy);
    AdjustLayout();
}







void CFileView::OnContextMenu(CWnd* pWnd, CPoint point)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*)&m_wndFileView;
    ASSERT_VALID(pWndTree);

    if (pWnd != pWndTree)
    {
        CDockablePane::OnContextMenu(pWnd, point);
        return;
    }

    if (point != CPoint(-1, -1))
    {
        // 选择已单击的项: 
        CPoint ptTree = point;
        pWndTree->ScreenToClient(&ptTree);

        UINT flags = 0;
        HTREEITEM hTreeItem = pWndTree->HitTest(ptTree, &flags);
        if (hTreeItem != nullptr)
        {
            pWndTree->SelectItem(hTreeItem);
        }
    }

    pWndTree->SetFocus();
    theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EXPLORER, point.x, point.y, this, TRUE);
}



void CFileView::AdjustLayout()
{
    if (GetSafeHwnd() == nullptr)
    {
        return;
    }

    CRect rectClient;
    GetClientRect(rectClient);

    int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

    m_wndToolBar.SetWindowPos(nullptr, rectClient.left, rectClient.top, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
    m_wndFileView.SetWindowPos(nullptr, rectClient.left + 1, rectClient.top + cyTlb + 1, rectClient.Width() - 2, rectClient.Height() - cyTlb - 2, SWP_NOACTIVATE | SWP_NOZORDER);
}



void CFileView::OnProperties()
{
    AfxMessageBox(_T("属性...."));

}



void CFileView::OnFileOpen()
{
    // TODO: 在此处添加命令处理程序代码
    HTREEITEM   hCurSelectedItem = m_wndFileView.GetSelectedItem();


    //if selected item has child item,it is not the last item,don open the doucument
    if (m_wndFileView.ItemHasChildren(hCurSelectedItem))
    {
        m_wndFileView.Expand(hCurSelectedItem, TVE_EXPAND);
    }
    else
    {
        activeSelectedDocumentTab();
    }
}



void CFileView::OnFileOpenWith()
{
    // TODO: 在此处添加命令处理程序代码
}



void CFileView::OnDummyCompile()
{
    // TODO: 在此处添加命令处理程序代码
}

void CFileView::OnEditCut()
{
    // TODO: 在此处添加命令处理程序代码
}



void CFileView::OnEditCopy()
{
    // TODO: 在此处添加命令处理程序代码
}



void CFileView::OnEditClear()
{
    // TODO: 在此处添加命令处理程序代码
}



void CFileView::OnPaint()
{
    CPaintDC dc(this); // 用于绘制的设备上下文
    CRect rectTree;
    m_wndFileView.GetWindowRect(rectTree);
    ScreenToClient(rectTree);

    rectTree.InflateRect(1, 1);
    dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}



void CFileView::OnSetFocus(CWnd* pOldWnd)
{
    CDockablePane::OnSetFocus(pOldWnd);

    m_wndFileView.SetFocus();
}



void CFileView::OnChangeVisualStyle()
{
    m_wndToolBar.CleanUpLockedImages();
    m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_EXPLORER_24 : IDR_EXPLORER, 0, 0, TRUE /* 锁定*/);

    m_FileViewImages.DeleteImageList();

    UINT uiBmpId = theApp.m_bHiColorIcons ? IDB_FILE_VIEW_24 : IDB_FILE_VIEW;

    CBitmap bmp;
    if (!bmp.LoadBitmap(uiBmpId))
    {
        TRACE(_T("无法加载位图: %x\n"), uiBmpId);
        ASSERT(FALSE);
        return;
    }

    BITMAP bmpObj;
    bmp.GetBitmap(&bmpObj);

    UINT nFlags = ILC_MASK;

    nFlags |= (theApp.m_bHiColorIcons) ? ILC_COLOR24 : ILC_COLOR4;

    m_FileViewImages.Create(16, bmpObj.bmHeight, nFlags, 0, 0);
    m_FileViewImages.Add(&bmp, RGB(255, 0, 255));

    m_wndFileView.SetImageList(&m_FileViewImages, TVSIL_NORMAL);
}



CViewTree* CFileView::getMyViewTree()
{
    // TODO: 在此处添加实现代码.
    return &m_wndFileView;
}



void CFileView::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
    CDockablePane::OnSettingChange(uFlags, lpszSection);

    // TODO: 在此处添加消息处理程序代码
}


void CFileView::activeSelectedDocumentTab()
{
    HTREEITEM   hCurSelectedItem = m_wndFileView.GetSelectedItem();
    CString     strSelectedItemTitle = m_wndFileView.GetItemText(hCurSelectedItem);
    CString     strSelectedItemPathName = m_wndFileView.GetDocumentItemPathName(hCurSelectedItem);
    CString     strDocTitle = _T("");
    CString     strDocPathName = _T("");
    const  CObList& tabGroups = ((CMainFrame*)AfxGetMainWnd())->GetMDITabGroups();

    if (tabGroups.GetCount() == 0)
    {
        return;
    }


    CMFCTabCtrl* pTabCtrl = (CMFCTabCtrl*)tabGroups.GetHead();
    for (int i = 0; i < pTabCtrl->GetTabsNum(); i++)
    {
        CChildFrame* pChildWnd = dynamic_cast <CChildFrame*>(pTabCtrl->GetTabWnd(i));
        strDocTitle = pChildWnd->GetActiveDocument()->GetTitle();
        strDocPathName = pChildWnd->GetActiveDocument()->GetPathName();


        //// Active same title document tab
        //if (!strSelectedItemTitle.Compare(strDocTitle) && strDocPathName.GetLength() < strDocTitle.GetLength())
        //{
        //    pTabCtrl->SetActiveTab(i);
        //    return;
        //}


        //// Active same pathname document tab
        //if (!strSelectedItemTitle.Compare(strDocPathName))
        //{
        //    pTabCtrl->SetActiveTab(i);
        //    return;
        //}


        //if (!strSelectedItemTitle.Compare(strDocTitle) && strDocPathName.GetLength() >= strDocTitle.GetLength())
        //{
        //    pTabCtrl->SetActiveTab(i);
        //    return;
        //}



        // Active same title document tab
        if (!strSelectedItemTitle.Compare(strDocTitle) && !strSelectedItemPathName.Compare(strDocPathName))
        {
            pTabCtrl->SetActiveTab(i);
            return;
        }
    }
}


void CFileView::fillViewTree()
{
    // TODO: 在此处添加实现代码.
    getMyViewTree()->InitViewTree();
}