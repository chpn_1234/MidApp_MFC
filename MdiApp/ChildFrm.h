﻿/** 
* @file     ChildFrm.h
* @brief    项目主函数文件
* @details  主要包含协议应用栈程序框架，main函数入口
* @author   somebody  any question please send mail to somebody@poissonsoft.com
* @date     2023-01-29
* @version  V1.0
* @copyright  Copyright (c) 2022-2025  深圳泊松软件技术有限公司
**********************************************************************************
* @attention
* SDK版本：nRF1_SDK_1.0.0
* @par 修改日志:
* <table>
* <tr><th>Date   <th>Version  <th>Author  <th>Description
* <tr><td>2022-10-26  <td>1.0 <td> somebody  <td>创建初始版本
* </table>
*
*/

#pragma once

class CChildFrame: public CMDIChildWndEx
{
    DECLARE_DYNCREATE(CChildFrame)
public:
    CChildFrame() noexcept;

    // 特性
protected:
    CSplitterWndEx m_wndSplitter;
public:

    // 操作
public:

    // 重写
    virtual BOOL PreCreateWindow(CREATESTRUCT &cs);

    // 实现
public:
    virtual ~CChildFrame();
#ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext &dc) const;
#endif


    // 生成的消息映射函数
protected:
    afx_msg void OnFilePrint();
    afx_msg void OnFilePrintPreview();
    afx_msg void OnUpdateFilePrintPreview(CCmdUI *pCmdUI);
    afx_msg void OnMDIActivate(BOOL bActivate, CWnd *pActivateWnd, CWnd *pDeactivateWnd);
    DECLARE_MESSAGE_MAP()



public:
    virtual void ActivateFrame(int nCmdShow = -1);
    virtual BOOL DestroyWindow();
    virtual void PostNcDestroy();
};
