﻿// 这段 MFC 示例源代码演示如何使用 MFC Microsoft Office Fluent 用户界面
// (“Fluent UI”)。该示例仅供参考，
// 用以补充《Microsoft 基础类参考》和
// MFC C++ 库软件随附的相关电子文档。
// 复制、使用或分发 Fluent UI 的许可条款是单独提供的。
// 若要了解有关 Fluent UI 许可计划的详细信息，请访问
// https://go.microsoft.com/fwlink/?LinkId=238214.
//
// 版权所有(C) Microsoft Corporation
// 保留所有权利。

// MainFrm.cpp: CMainFrame 类的实现
//

#include "pch.h"
#include "framework.h"
#include "MdiApp.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "Resource.h"





HBITMAP ConvertIconToBitmap(HICON  hIcon)
{
    HBITMAP   hBmp;
    BITMAP   bmp;
    CDC   bmpDC;
    CDC   iconDC;
    ICONINFO         csII;
    int bRetValue   =   ::GetIconInfo(hIcon, &csII);
    if(bRetValue   ==   FALSE)   return   NULL;
    bmpDC.Attach(::GetDC(NULL));
    iconDC.CreateCompatibleDC(&bmpDC);
    if(::GetObject(csII.hbmColor, sizeof(BITMAP), &bmp))
    {
        DWORD       dwWidth   =   24;
        DWORD       dwHeight   =   24;
        hBmp=   ::CreateBitmap(dwWidth, dwHeight, bmp.bmPlanes,32, NULL);
        iconDC.SelectObject(csII.hbmColor);
        bmpDC.SelectObject(hBmp);
        bmpDC.BitBlt(0, 0, dwWidth, dwHeight, &iconDC, 0, 0, SRCCOPY);
        return   hBmp;
    }
    return NULL;
}









#ifdef _DEBUG
#define new DEBUG_NEW
#endif







// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWndEx)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWndEx)
    ON_WM_CREATE()
    ON_COMMAND(ID_WINDOW_MANAGER, &CMainFrame::OnWindowManager)
    ON_COMMAND_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnApplicationLook)
    ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnUpdateApplicationLook)
    ON_COMMAND(ID_VIEW_CAPTION_BAR, &CMainFrame::OnViewCaptionBar)
    ON_UPDATE_COMMAND_UI(ID_VIEW_CAPTION_BAR, &CMainFrame::OnUpdateViewCaptionBar)
    ON_COMMAND(ID_TOOLS_OPTIONS, &CMainFrame::OnOptions)
    ON_REGISTERED_MESSAGE(AFX_WM_CHANGE_ACTIVE_TAB, &CMainFrame::OnActivateTabChanged)
    ON_WM_SETTINGCHANGE()
    ON_COMMAND(ID_BUTTON_PS_SAVE_DOCUMENT, &CMainFrame::PSsaveCurrentDocument)
    ON_COMMAND(ID_BUTTON2, &CMainFrame::onRibbonButton2)
    ON_COMMAND(ID_SLIDER1, &CMainFrame::onTestRibbonSilder1ValueChanged)
    ON_COMMAND(ID_COMBO2, &CMainFrame::onTestRibbonComboBox2SelectedChanged)
    ON_COMMAND(ID_RIBBON_CONTROL_TEST_BUTTON1, &CMainFrame::onClickTestRibbonButton1)
    ON_UPDATE_COMMAND_UI(ID_RIBBON_CONTROL_TEST_BUTTON2, &CMainFrame::onTestRibbonButton2)
    ON_COMMAND(ID_RIBBON_CONTROL_TEST_BUTTON2_MENU1, &CMainFrame::onClickTestRibbonButton1)
    ON_COMMAND(ID_RIBBON_CONTROL_TEST_BUTTON2_MENU2, &CMainFrame::onClickTestRibbonButton2Menu2)
END_MESSAGE_MAP()




// CMainFrame 构造/析构

CMainFrame::CMainFrame() noexcept
{
    // TODO: 在此添加成员初始化代码
    theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_OFF_2007_BLUE);
}


CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if(CMDIFrameWndEx::OnCreate(lpCreateStruct) == -1)
    {
        return -1;
    }


    BOOL bNameValid;

    CMDITabInfo mdiTabParams;
    mdiTabParams.m_style                    = CMFCTabCtrl::STYLE_3D_ONENOTE;    // 其他可用样式...
    mdiTabParams.m_bActiveTabCloseButton    = TRUE;                             // 设置为 FALSE 会将关闭按钮放置在选项卡区域的右侧
    mdiTabParams.m_bTabIcons                = TRUE;                            // 设置为 TRUE 将在 MDI 选项卡上启用文档图标
    mdiTabParams.m_bAutoColor               = TRUE;                             // 设置为 FALSE 将禁用 MDI 选项卡的自动着色
    mdiTabParams.m_bDocumentMenu            = TRUE;                             // 在选项卡区域的右边缘启用文档菜单
    EnableMDITabbedGroups(TRUE, mdiTabParams);                                  // Set Style

   
    /*
    * Create RibbonBar
    * Init Ribbon control
    */
    {
        m_wndRibbonBar.Create(this);
        m_wndRibbonBar.LoadFromResource(IDR_RIBBON);

        //{
        ////设置应用键图标
        //    auto pAppButton =  m_wndRibbonBar.GetApplicationButton();
        //    HICON  hicon = AfxGetApp()->LoadIcon(IDI_ICON1);
        //    HBITMAP hbitmap = ConvertIconToBitmap(hicon);

        //    if(hbitmap)
        //    {
        //        pAppButton->SetImage(hbitmap);
        //    }
        //   
        //}
      



        CMFCRibbonComboBox *pRibbonComboBox = DYNAMIC_DOWNCAST(CMFCRibbonComboBox, m_wndRibbonBar.FindByID(ID_COMBO2));
        ASSERT_VALID(pRibbonComboBox);

        CString str1 = _T("test 1");
        CString str3 = _T("test 3");
        CString str2 = _T("test 2");

        //init Combobox2
        pRibbonComboBox->AddItem(str1);
        pRibbonComboBox->AddItem(str2);
        pRibbonComboBox->AddItem(str3);
        pRibbonComboBox->SelectItem(0);


        //init Slider1
        CMFCRibbonSlider *pRibbonSlider = DYNAMIC_DOWNCAST(CMFCRibbonSlider, m_wndRibbonBar.FindByID(ID_SLIDER1));
        ASSERT_VALID(pRibbonSlider);
        int             nSliderPos      = 40;
        pRibbonSlider->SetPos(nSliderPos);
        pRibbonSlider->SetToolTipText(_T("Slider1"));


        //init Progress1
        CMFCRibbonProgressBar *pRibbonProgressBar = DYNAMIC_DOWNCAST(CMFCRibbonProgressBar, m_wndRibbonBar.FindByID(ID_PROGRESS1));
        ASSERT_VALID(pRibbonProgressBar);
        pRibbonProgressBar->SetPos(nSliderPos);
        pRibbonProgressBar->SetToolTipText(_T("show slider1 pos"));


        //init Edit2
        CMFCRibbonEdit *pRibbonEdit = DYNAMIC_DOWNCAST(CMFCRibbonEdit, m_wndRibbonBar.FindByID(ID_EDIT2));
        ASSERT_VALID(pRibbonEdit);
        CString strEditText = _T("");
        strEditText.Format(_T("Silder1 pos = %d"), nSliderPos);
        pRibbonEdit->SetEditText(strEditText);
        pRibbonEdit->SetToolTipText(_T("show slider1 pos"));
        pRibbonEdit->SetVisible(FALSE);


        //Creat a ribbon category by  pure code 
        {
            CMFCRibbonCategory *pRibbonCategory     =  m_wndRibbonBar.AddCategory(_T("Category  whitch is created by code"), IDB_MAIN, IDB_MAIN);
            CMFCRibbonPanel *pRibbonPanel1          = pRibbonCategory->AddPanel(_T("panel1"));
            CMFCRibbonPanel *pRibbonPanel2          = pRibbonCategory->AddPanel(_T("panel2"));


            /*
            * Add a CMFCRibbonButton
            * name =  "test button1"
            * "test button1" is just a simple click button
            */
            {
                HICON iconOfRibbonButton1            = AfxGetApp()->LoadIconW(IDR_MAINFRAME);
                CMFCRibbonButton *pRibbonButton1     = new  CMFCRibbonButton(ID_RIBBON_CONTROL_TEST_BUTTON1, _T("test button1"), iconOfRibbonButton1, TRUE);
                pRibbonButton1->SetToolTipText(_T("test button1 is created by code"));
                pRibbonPanel1->Add(pRibbonButton1);
                pRibbonPanel1->AddSeparator();          //add a separator
            }


            /*
            * Add a CMFCRibbonButton
            * name =  "test button2"
            * "test button2" has a menu
            */
            {
                HICON iconOfRibbonButton2            = AfxGetApp()->LoadIconW(IDR_MdiAppTYPE);
                CMFCRibbonButton *pRibbonButton2     = new  CMFCRibbonButton(ID_RIBBON_CONTROL_TEST_BUTTON2, _T("test button2"), iconOfRibbonButton2, TRUE);
                pRibbonButton2->SetToolTipText(_T("test button2 is created by code"));
                pRibbonPanel1->Add(pRibbonButton2);


                //create and add menu
                CMenu menuOfRibbonButton2;
                menuOfRibbonButton2.CreateMenu();
                menuOfRibbonButton2.AppendMenuW(MF_STRING, ID_RIBBON_CONTROL_TEST_BUTTON2_MENU1, _T("clicke test button1"));
                menuOfRibbonButton2.AppendMenuW(MF_STRING, ID_RIBBON_CONTROL_TEST_BUTTON2_MENU2, _T("show message box"));
                pRibbonButton2->SetMenu(menuOfRibbonButton2.GetSafeHmenu());
            }

        }
    }


    //Open Status Bar
    if(!m_wndStatusBar.Create(this))
    {
        TRACE0("未能创建状态栏\n");
        return -1;      // 未能创建
    }

    CString strTitlePane1;
    CString strTitlePane2;
    bNameValid = strTitlePane1.LoadString(IDS_STATUS_PANE1);
    ASSERT(bNameValid);
    bNameValid = strTitlePane2.LoadString(IDS_STATUS_PANE2);
    ASSERT(bNameValid);
    m_wndStatusBar.AddElement(new CMFCRibbonStatusBarPane(ID_STATUSBAR_PANE1, strTitlePane1, TRUE), strTitlePane1);
    m_wndStatusBar.AddExtendedElement(new CMFCRibbonStatusBarPane(ID_STATUSBAR_PANE2, strTitlePane2, TRUE), strTitlePane2);


    // 启用 Visual Studio 2005 样式停靠窗口行为
    CDockingManager::SetDockingMode(DT_SMART);
    // 启用 Visual Studio 2005 样式停靠窗口自动隐藏行为
    //EnableAutoHidePanes(CBRS_ALIGN_ANY);


    // 导航窗格将创建在左侧，因此将暂时禁用左侧的停靠: 
    EnableDocking(CBRS_ALIGN_TOP | CBRS_ALIGN_BOTTOM | CBRS_ALIGN_RIGHT);


    // 创建并设置“Outlook”导航栏: 
    if(!CreateOutlookBar(m_wndNavigationBar, ID_VIEW_NAVIGATION, m_wndTree, m_wndCalendar, 250))
    {
        TRACE0("未能创建导航窗格\n");
        return -1;      // 未能创建
    }

    // 创建标题栏: 
    if(!CreateCaptionBar())
    {
        TRACE0("未能创建标题栏\n");
        return -1;      // 未能创建
    }

    // 已创建 Outlook 栏，应允许在左侧停靠。
    EnableDocking(CBRS_ALIGN_LEFT);
    //EnableAutoHidePanes(CBRS_ALIGN_RIGHT);


    // 加载菜单项图像(不在任何标准工具栏上): 
    CMFCToolBar::AddToolBarForImageCollection(IDR_MENU_IMAGES, theApp.m_bHiColorIcons ? IDB_MENU_IMAGES_24 : 0);


    // 创建停靠窗口
    if(!CreateDockingWindows())
    {
        TRACE0("未能创建停靠窗口\n");
        return -1;
    }


    m_wndFileView.EnableDocking(CBRS_ALIGN_LEFT);
    m_wndClassView.EnableDocking(CBRS_ALIGN_LEFT);
    DockPane(&m_wndFileView);
    CDockablePane *pTabbedBar = nullptr;
    m_wndClassView.AttachToTabWnd(&m_wndFileView, DM_SHOW, TRUE, &pTabbedBar);

    m_wndOutput.EnableDocking(CBRS_ALIGN_BOTTOM);
    DockPane(&m_wndOutput);

    m_wndProperties.EnableDocking(CBRS_ALIGN_RIGHT);
    DockPane(&m_wndProperties);

    /* pTabbedBar->SetControlBarStyle(AFX_CBRS_RESIZE);*/
   /* m_wndFileView.SetControlBarStyle(AFX_CBRS_RESIZE);
    m_wndClassView.SetControlBarStyle(AFX_CBRS_RESIZE);
    m_wndOutput.SetControlBarStyle(AFX_CBRS_RESIZE);
    m_wndProperties.SetControlBarStyle(AFX_CBRS_RESIZE);*/

    GetDockingManager()->DisableRestoreDockState();

    // 基于持久值设置视觉管理器和样式
    OnApplicationLook(theApp.m_nAppLook);


    // 启用增强的窗口管理对话框
    EnableWindowsDialog(ID_WINDOW_MANAGER, ID_WINDOW_MANAGER, TRUE);


    // 将文档名和应用程序名称在窗口标题栏上的顺序进行交换。这
    // 将改进任务栏的可用性，因为显示的文档名带有缩略图。
    ModifyStyle(0, FWS_PREFIXTITLE);

    return 0;
}



BOOL CMainFrame::PreCreateWindow(CREATESTRUCT &cs)
{
    if(!CMDIFrameWndEx::PreCreateWindow(cs))
        return FALSE;
    // TODO: 在此处通过修改
    //  CREATESTRUCT cs 来修改窗口类或样式


    return TRUE;
}



BOOL CMainFrame::CreateDockingWindows()
{
    BOOL bNameValid;

    // 创建类视图
    CString strClassView;
    bNameValid = strClassView.LoadString(IDS_CLASS_VIEW);
    ASSERT(bNameValid);
    if(!m_wndClassView.Create(strClassView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_CLASSVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI, AFX_CBRS_OUTLOOK_TABS, AFX_CBRS_RESIZE))
    {
        TRACE0("未能创建“类视图”窗口\n");
        return FALSE; // 未能创建
    }


    // 创建文件视图
    CString strFileView;
    bNameValid = strFileView.LoadString(IDS_FILE_VIEW);
    ASSERT(bNameValid);
    if(!m_wndFileView.Create(strFileView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_FILEVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI, AFX_CBRS_REGULAR_TABS, AFX_CBRS_RESIZE))
    {
        TRACE0("未能创建“文件视图”窗口\n");
        return FALSE; // 未能创建
    }


    // 创建输出窗口
    CString strOutputWnd;
    bNameValid = strOutputWnd.LoadString(IDS_OUTPUT_WND);
    ASSERT(bNameValid);
    if(!m_wndOutput.Create(strOutputWnd, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_OUTPUTWND, WS_CHILD | WS_VISIBLE | CBRS_BOTTOM | CBRS_HIDE_INPLACE | WS_CAPTION/*, AFX_CBRS_REGULAR_TABS, AFX_CBRS_RESIZE*/))
    {
        TRACE0("未能创建输出窗口\n");
        return FALSE; // 未能创建
    }


    // 创建属性窗口
    CString strPropertiesWnd;
    bNameValid = strPropertiesWnd.LoadString(IDS_PROPERTIES_WND);
    ASSERT(bNameValid);
    if(!m_wndProperties.Create(strPropertiesWnd, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_PROPERTIESWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_RIGHT | CBRS_FLOAT_MULTI, AFX_CBRS_REGULAR_TABS, AFX_CBRS_RESIZE))
    {
        TRACE0("未能创建“属性”窗口\n");
        return FALSE; // 未能创建
    }


    SetDockingWindowIcons(theApp.m_bHiColorIcons);
    return TRUE;
}



void CMainFrame::SetDockingWindowIcons(BOOL bHiColorIcons)
{
    HICON hFileViewIcon = ( HICON ) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_FILE_VIEW_HC : IDI_FILE_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndFileView.SetIcon(hFileViewIcon, FALSE);


    HICON hClassViewIcon = ( HICON ) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_CLASS_VIEW_HC : IDI_CLASS_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndClassView.SetIcon(hClassViewIcon, FALSE);


    HICON hOutputBarIcon = ( HICON ) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_OUTPUT_WND_HC : IDI_OUTPUT_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndOutput.SetIcon(hOutputBarIcon, FALSE);


    HICON hPropertiesBarIcon = ( HICON ) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_PROPERTIES_WND_HC : IDI_PROPERTIES_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndProperties.SetIcon(hPropertiesBarIcon, FALSE);

    UpdateMDITabbedBarsIcons();
}



BOOL CMainFrame::CreateOutlookBar(CMFCOutlookBar &bar, UINT uiID, CMFCShellTreeCtrl &tree, CCalendarBar &calendar, int nInitialWidth)
{
    bar.SetMode2003();

    BOOL bNameValid;
    CString strTemp;
    bNameValid = strTemp.LoadString(IDS_SHORTCUTS);
    ASSERT(bNameValid);
    if(!bar.Create(strTemp, this, CRect(0, 0, nInitialWidth, 32000), uiID, WS_CHILD | WS_VISIBLE | CBRS_LEFT))
    {
        return FALSE; // 未能创建
    }

    CMFCOutlookBarTabCtrl *pOutlookBar = (CMFCOutlookBarTabCtrl *)bar.GetUnderlyingWindow();

    if(pOutlookBar == nullptr)
    {
        ASSERT(FALSE);
        return FALSE;
    }

    pOutlookBar->EnableInPlaceEdit(TRUE);

    static UINT uiPageID = 1;

    // 可浮动，可自动隐藏，可调整大小，但不能关闭
    DWORD dwStyle = AFX_CBRS_FLOAT | AFX_CBRS_AUTOHIDE | AFX_CBRS_RESIZE;

    CRect rectDummy(0, 0, 0, 0);
    const DWORD dwTreeStyle = WS_CHILD | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS;

    tree.Create(dwTreeStyle, rectDummy, &bar, 1200);
    bNameValid = strTemp.LoadString(IDS_FOLDERS);
    ASSERT(bNameValid);
    pOutlookBar->AddControl(&tree, strTemp, 2, TRUE, dwStyle);

    calendar.Create(rectDummy, &bar, 1201);
    bNameValid = strTemp.LoadString(IDS_CALENDAR);
    ASSERT(bNameValid);
    pOutlookBar->AddControl(&calendar, strTemp, 3, TRUE, dwStyle);

    bar.SetPaneStyle(bar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

    pOutlookBar->SetImageList(theApp.m_bHiColorIcons ? IDB_PAGES_HC : IDB_PAGES, 24);
    pOutlookBar->SetToolbarImageList(theApp.m_bHiColorIcons ? IDB_PAGES_SMALL_HC : IDB_PAGES_SMALL, 16);
    pOutlookBar->RecalcLayout();

    BOOL bAnimation = theApp.GetInt(_T("OutlookAnimation"), TRUE);
    CMFCOutlookBarTabCtrl::EnableAnimation(bAnimation);

    bar.SetButtonsFont(&afxGlobalData.fontBold);

    return TRUE;
}



BOOL CMainFrame::CreateCaptionBar()
{
    if(!m_wndCaptionBar.Create(WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS, this, ID_VIEW_CAPTION_BAR, -1, TRUE))
    {
        TRACE0("未能创建标题栏\n");
        return FALSE;
    }

    BOOL bNameValid;

    CString strTemp, strTemp2;
    bNameValid = strTemp.LoadString(IDS_CAPTION_BUTTON);
    ASSERT(bNameValid);
    m_wndCaptionBar.SetButton(strTemp, ID_TOOLS_OPTIONS, CMFCCaptionBar::ALIGN_LEFT, FALSE);
    bNameValid = strTemp.LoadString(IDS_CAPTION_BUTTON_TIP);
    ASSERT(bNameValid);
    m_wndCaptionBar.SetButtonToolTip(strTemp);

    bNameValid = strTemp.LoadString(IDS_CAPTION_TEXT);
    ASSERT(bNameValid);
    m_wndCaptionBar.SetText(strTemp, CMFCCaptionBar::ALIGN_LEFT);

    m_wndCaptionBar.SetBitmap(IDB_INFO, RGB(255, 255, 255), FALSE, CMFCCaptionBar::ALIGN_LEFT);
    bNameValid = strTemp.LoadString(IDS_CAPTION_IMAGE_TIP);
    ASSERT(bNameValid);
    bNameValid = strTemp2.LoadString(IDS_CAPTION_IMAGE_TEXT);
    ASSERT(bNameValid);
    m_wndCaptionBar.SetImageToolTip(strTemp, strTemp2);

    return TRUE;
}



// CMainFrame 诊断

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
    CMDIFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext &dc) const
{
    CMDIFrameWndEx::Dump(dc);
}
#endif //_DEBUG


// CMainFrame 消息处理程序

void CMainFrame::OnWindowManager()
{
    ShowWindowsDialog();
}

void CMainFrame::OnApplicationLook(UINT id)
{
    CWaitCursor wait;

    theApp.m_nAppLook = id;

    switch(theApp.m_nAppLook)
    {
    case ID_VIEW_APPLOOK_WIN_2000:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManager));
        m_wndRibbonBar.SetWindows7Look(FALSE);
        break;

    case ID_VIEW_APPLOOK_OFF_XP:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
        m_wndRibbonBar.SetWindows7Look(FALSE);
        break;

    case ID_VIEW_APPLOOK_WIN_XP:
        CMFCVisualManagerWindows::m_b3DTabsXPTheme = TRUE;
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
        m_wndRibbonBar.SetWindows7Look(FALSE);
        break;

    case ID_VIEW_APPLOOK_OFF_2003:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2003));
        CDockingManager::SetDockingMode(DT_SMART);
        m_wndRibbonBar.SetWindows7Look(FALSE);
        break;

    case ID_VIEW_APPLOOK_VS_2005:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2005));
        CDockingManager::SetDockingMode(DT_SMART);
        m_wndRibbonBar.SetWindows7Look(FALSE);
        break;

    case ID_VIEW_APPLOOK_VS_2008:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2008));
        CDockingManager::SetDockingMode(DT_SMART);
        m_wndRibbonBar.SetWindows7Look(FALSE);
        break;

    case ID_VIEW_APPLOOK_WINDOWS_7:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows7));
        CDockingManager::SetDockingMode(DT_SMART);
        m_wndRibbonBar.SetWindows7Look(TRUE);
        break;

    default:
        switch(theApp.m_nAppLook)
        {
        case ID_VIEW_APPLOOK_OFF_2007_BLUE:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_LunaBlue);
            break;

        case ID_VIEW_APPLOOK_OFF_2007_BLACK:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
            break;

        case ID_VIEW_APPLOOK_OFF_2007_SILVER:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
            break;

        case ID_VIEW_APPLOOK_OFF_2007_AQUA:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Aqua);
            break;
        }

        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
        CDockingManager::SetDockingMode(DT_SMART);
        m_wndRibbonBar.SetWindows7Look(FALSE);
    }

    m_wndOutput.UpdateFonts();
    RedrawWindow(nullptr, nullptr, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_FRAME | RDW_ERASE);

    theApp.WriteInt(_T("ApplicationLook"), theApp.m_nAppLook);
}

void CMainFrame::OnUpdateApplicationLook(CCmdUI *pCmdUI)
{
    pCmdUI->SetRadio(theApp.m_nAppLook == pCmdUI->m_nID);
}

void CMainFrame::OnViewCaptionBar()
{
    m_wndCaptionBar.ShowWindow(m_wndCaptionBar.IsVisible() ? SW_HIDE : SW_SHOW);
    RecalcLayout(FALSE);
}

void CMainFrame::OnUpdateViewCaptionBar(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(m_wndCaptionBar.IsVisible());
}

void CMainFrame::OnOptions()
{
    CMFCRibbonCustomizeDialog *pOptionsDlg = new CMFCRibbonCustomizeDialog(this, &m_wndRibbonBar);
    ASSERT(pOptionsDlg != nullptr);

    pOptionsDlg->DoModal();
    delete pOptionsDlg;
}


void CMainFrame::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
    CMDIFrameWndEx::OnSettingChange(uFlags, lpszSection);
    m_wndOutput.UpdateFonts();
}



void CMainFrame::updateMyFileView()
{
    if(m_wndFileView.getMyViewTree())
    {
        m_wndFileView.getMyViewTree()->InitViewTree();
    }
}




BOOL CMainFrame::OnShowPopupMenu(CMFCPopupMenu *pMenuPopup)
{
    // TODO: 在此添加专用代码和/或调用基类

    return CMDIFrameWndEx::OnShowPopupMenu(pMenuPopup);
}


BOOL CMainFrame::OnShowMDITabContextMenu(CPoint point, DWORD dwAllowedItems, BOOL bTabDrop)
{
    // TODO: 在此添加专用代码和/或调用基类

    return CMDIFrameWndEx::OnShowMDITabContextMenu(point, dwAllowedItems, bTabDrop);
}


LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    // TODO: 在此添加专用代码和/或调用基类

    return CMDIFrameWndEx::WindowProc(message, wParam, lParam);
}



LRESULT CMainFrame::OnActivateTabChanged(WPARAM wParam, LPARAM lParam)
{
    CMFCBaseTabCtrl *pTabCtrl = ( (CMFCBaseTabCtrl *)lParam );

    if(pTabCtrl && pTabCtrl->GetSafeHwnd())
    {
        int nTabIdx     = pTabCtrl->GetActiveTab();
        CWnd *pWnd      = pTabCtrl->GetActiveWnd();
        if(pWnd && pWnd->GetSafeHwnd())
        {
            pWnd->SetForegroundWindow();//激活当前tab
        }
    }
    return 0;
}




void CMainFrame::PSsaveCurrentDocument()
{
    const  CObList &tabGroups   = ( (CMainFrame *)AfxGetMainWnd() )->GetMDITabGroups();
    CString         strCaption  = _T("");
    CString         strText     = _T("");

    if(tabGroups.GetCount() == 0)
    {
        return;
    }

    /*  CMFCTabCtrl *pTabCtrl = (CMFCTabCtrl *)tabGroups.GetHead();
      CChildFrame *pChildWnd  =  dynamic_cast <CChildFrame *>(pTabCtrl->GetTabWnd(pTabCtrl->GetActiveTab()));
      if (pChildWnd)
      {
          strDocumentTitle             =  pChildWnd->GetActiveDocument()->GetTitle();
          strDocumentPathName          =  pChildWnd->GetActiveDocument()->GetPathName();
          MessageBox(strDocumentPathName, strDocumentTitle);
          pChildWnd->GetActiveDocument()->DoFileSave();
          UpdateMyFileView();
      }*/



    CChildFrame *pChildWnd  = dynamic_cast <CChildFrame *>( GetActiveFrame() );
    CDocument *pDocument    =   nullptr;
    if(pChildWnd)
    {
        pDocument = pChildWnd->GetActiveDocument();
        if(pDocument)
        {
            strCaption              =  pChildWnd->GetActiveDocument()->GetTitle();
            strText                 =  _T("do you want to save file ") + strCaption + _T(" ?");
            auto messageBoxReturn   =  MessageBox(strText, strCaption, MB_OKCANCEL);

            if(messageBoxReturn == IDOK)
            {
                pDocument->DoFileSave();
                updateMyFileView();
            }
            else
            {
                return;
            }

        }
    }
}


void CMainFrame::onRibbonButton2()
{
    MessageBox(_T("Button2 is created by ribbon creator"));
}


void CMainFrame::onTestRibbonSilder1ValueChanged()
{
    CMFCRibbonBar *pRibbon = ( (CMDIFrameWndEx *)AfxGetMainWnd() )->GetRibbonBar();
    ASSERT_VALID(pRibbon);

    CMFCRibbonSlider *pRibbonSlider = DYNAMIC_DOWNCAST(CMFCRibbonSlider, pRibbon->FindByID(ID_SLIDER1));
    ASSERT_VALID(pRibbonSlider);

    CMFCRibbonProgressBar *pRibbonProgressBar = DYNAMIC_DOWNCAST(CMFCRibbonProgressBar, pRibbon->FindByID(ID_PROGRESS1));
    ASSERT_VALID(pRibbonProgressBar);

    CMFCRibbonEdit *pRibbonEdit = DYNAMIC_DOWNCAST(CMFCRibbonEdit, pRibbon->FindByID(ID_EDIT2));
    ASSERT_VALID(pRibbonEdit);



    pRibbonSlider->SetZoomIncrement(1);
    int nSliderPos = pRibbonSlider->GetPos();


    pRibbonProgressBar->SetPos(nSliderPos);


    CString str = _T("0");
    str.Format(_T("Silder1 pos = %d"), nSliderPos);
    pRibbonEdit->SetEditText(str);

}


void CMainFrame::onTestRibbonComboBox2SelectedChanged()
{
    CMFCRibbonBar *pRibbon = ( (CMDIFrameWndEx *)AfxGetMainWnd() )->GetRibbonBar();
    ASSERT_VALID(pRibbon);

    CMFCRibbonComboBox *pRibbonComboBox = DYNAMIC_DOWNCAST(CMFCRibbonComboBox, pRibbon->FindByID(ID_COMBO2));
    ASSERT_VALID(pRibbonComboBox);

    int nSelItem    =  pRibbonComboBox->GetCurSel();
    CString str     = _T("");
    str.Format(_T("selected test %d"), nSelItem + 1);
    MessageBox(str);
}


void CMainFrame::onTestRibbonButton2(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(TRUE);
}


void CMainFrame::onClickTestRibbonButton1()
{
    MessageBox(_T("test buuton1 is created by code"));
}


void CMainFrame::onClickTestRibbonButton2Menu2()
{
    MessageBox(_T("clicked Menu2"), _T("message box:"));
}