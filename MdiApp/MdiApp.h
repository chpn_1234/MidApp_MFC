﻿// 这段 MFC 示例源代码演示如何使用 MFC Microsoft Office Fluent 用户界面
// (“Fluent UI”)。该示例仅供参考，
// 用以补充《Microsoft 基础类参考》和
// MFC C++ 库软件随附的相关电子文档。
// 复制、使用或分发 Fluent UI 的许可条款是单独提供的。
// 若要了解有关 Fluent UI 许可计划的详细信息，请访问
// https://go.microsoft.com/fwlink/?LinkId=238214.
//
// 版权所有(C) Microsoft Corporation
// 保留所有权利。

// MdiApp.h: MdiApp 应用程序的主头文件
//
#pragma once

#ifndef __AFXWIN_H__
#error "在包含此文件之前包含 'pch.h' 以生成 PCH"
#endif

#include "resource.h"       // 主符号



enum class EFileDocumentType
{
    E_FILE_DOCUMENT_TYPE_MID = 0,
    E_FILE_DOCUMENT_TYPE_MYDOCUMENT1 = 1
};



// MdiAppApp:
// 有关此类的实现，请参阅 MdiApp.cpp
//

class MdiAppApp : public CWinAppEx
{
public:
    MdiAppApp() noexcept;


    // 重写
public:
    virtual BOOL InitInstance();
    virtual int ExitInstance();

    // 实现
    UINT  m_nAppLook;
    BOOL  m_bHiColorIcons;

    virtual void        PreLoadState();
    virtual void        LoadCustomState();
    virtual void        SaveCustomState();
    virtual CDocument *OpenDocumentFile( LPCTSTR lpszFileName );
    CDocManager *getAppDocumentManager();
    void                onCloseMyDocument();
    static EFileDocumentType m_FileDocumentType;


    afx_msg void OnAppAbout();
    afx_msg void onMyFileNew();
    afx_msg void onMdiDocumentType();
    afx_msg void onMyDocumentTyp1();
    DECLARE_MESSAGE_MAP()


private:


    CDocManager *m_pAppDocumentManager      = nullptr;
    CMultiDocTemplate *m_pMdiDocumentType   = nullptr;        //
    CMultiDocTemplate *m_pMyDocumentType1   = nullptr;        //
};

extern MdiAppApp theApp;
