﻿/*******************************************************************************
 * @file       MainFrm.h
 * @brief
 * @details
 * @author
 * @date       2023-02-15
 * @version    V1.0
 * @copyright  Copyright (c) 2022-2025  Poisson Software Technology LTD
 *******************************************************************************/




 // 这段 MFC 示例源代码演示如何使用 MFC Microsoft Office Fluent 用户界面 
 // (“Fluent UI”)。该示例仅供参考，
 // 用以补充《Microsoft 基础类参考》和 
 // MFC C++ 库软件随附的相关电子文档。  
 // 复制、使用或分发 Fluent UI 的许可条款是单独提供的。  
 // 若要了解有关 Fluent UI 许可计划的详细信息，请访问 
 // https://go.microsoft.com/fwlink/?LinkId=238214.
 //
 // 版权所有(C) Microsoft Corporation
 // 保留所有权利。

 // MainFrm.h: CMainFrame 类的接口
 //


#pragma once
#include "FileView.h"
#include "ClassView.h"
#include "OutputWnd.h"
#include "PropertiesWnd.h"
#include "CalendarBar.h"
#include "Resource.h"
#include <iostream>



#define BEGIN_GET_LOCAL_TIME() \
    {\
    int  a,b,c;\
    a = 5;\
    b = 6;\
    c = 7;\

#define END_GET_LOCAL_TIME(str) \
		TRACE("%d,%d,%d\n",a,b,c + str );\
	};


HBITMAP ConvertIconToBitmap(HICON  hIcon);




class COutlookBar: public CMFCOutlookBar
{
    virtual BOOL AllowShowOnPaneMenu() const
    {
        return TRUE;
    }


    virtual void GetPaneName(CString &strName) const
    {
        BOOL bNameValid = strName.LoadString(IDS_OUTLOOKBAR); ASSERT(bNameValid); if(!bNameValid) strName.Empty();
    }
};



class CMainFrame: public CMDIFrameWndEx
{
    DECLARE_DYNAMIC(CMainFrame)
public:
    CMainFrame() noexcept;

    // 特性
public:

    // 操作
public:

    // 重写
public:
    virtual BOOL PreCreateWindow(CREATESTRUCT &cs);

    // 实现
public:
    virtual ~CMainFrame();
#ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext &dc) const;
#endif



public:
    void            updateMyFileView();
    virtual BOOL    OnShowPopupMenu(CMFCPopupMenu *pMenuPopup);
    virtual BOOL    OnShowMDITabContextMenu(CPoint point, DWORD dwAllowedItems, BOOL bTabDrop);
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);



protected:  // 控件条嵌入成员
    CMFCRibbonBar				m_wndRibbonBar;
    CMFCRibbonApplicationButton m_MainButton;
    CMFCToolBarImages			m_PanelImages;

    CMFCRibbonStatusBar			m_wndStatusBar;
    CFileView					m_wndFileView;
    CClassView					m_wndClassView;
    COutputWnd					m_wndOutput;
    CPropertiesWnd				m_wndProperties;

    COutlookBar					m_wndNavigationBar;
    CMFCShellTreeCtrl			m_wndTree;
    CCalendarBar				m_wndCalendar;
    CMFCCaptionBar				m_wndCaptionBar;


    // 生成的消息映射函数
protected:
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnWindowManager();
    afx_msg void OnApplicationLook(UINT id);
    afx_msg void OnUpdateApplicationLook(CCmdUI *pCmdUI);
    afx_msg void OnViewCaptionBar();
    afx_msg void OnUpdateViewCaptionBar(CCmdUI *pCmdUI);
    afx_msg void OnOptions();
    afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
    afx_msg LRESULT OnActivateTabChanged(WPARAM, LPARAM);
    afx_msg void PSsaveCurrentDocument();
    afx_msg void onRibbonButton2();
    afx_msg void onTestRibbonSilder1ValueChanged();
    afx_msg void onTestRibbonComboBox2SelectedChanged();
    afx_msg void onTestRibbonButton2(CCmdUI *pCmdUI);
    afx_msg void onClickTestRibbonButton1();
    afx_msg void onClickTestRibbonButton2Menu2();
    DECLARE_MESSAGE_MAP();


    BOOL CreateDockingWindows();
    void SetDockingWindowIcons(BOOL bHiColorIcons);
    BOOL CreateOutlookBar(CMFCOutlookBar &bar, UINT uiID, CMFCShellTreeCtrl &tree, CCalendarBar &calendar, int nInitialWidth);
    BOOL CreateCaptionBar();

    int FindFocusedOutlookWnd(CMFCOutlookBarTabCtrl **ppOutlookWnd);

    CMFCOutlookBarTabCtrl *FindOutlookParent(CWnd *pWnd);
    CMFCOutlookBarTabCtrl *m_pCurrOutlookWnd;
    CMFCOutlookBarPane *m_pCurrOutlookPage;
};
