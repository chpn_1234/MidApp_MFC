﻿// DlgMyDocumentTypeSet.cpp: 实现文件
//

#include "pch.h"
#include "MdiApp.h"
#include "afxdialogex.h"
#include "DlgMyDocumentTypeSet.h"


// DlgMyDocumentTypeSet 对话框

IMPLEMENT_DYNAMIC(DlgMyDocumentTypeSet, CDialogEx)
DlgMyDocumentTypeSet::DlgMyDocumentTypeSet(CWnd* pParent /*=nullptr*/)
    : CDialogEx(IDD_DLG_MY_DOCUMENT_TYPE_SET, pParent)
{

}

DlgMyDocumentTypeSet::~DlgMyDocumentTypeSet()
{
}

void DlgMyDocumentTypeSet::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_COMBO_DOCUMENT_TYPE, m_crtlCombDocumentType);
}


BEGIN_MESSAGE_MAP(DlgMyDocumentTypeSet, CDialogEx)
    ON_BN_CLICKED(IDOK, &DlgMyDocumentTypeSet::OnBnClickedOk)
    ON_WM_PAINT()
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()




// DlgMyDocumentTypeSet 消息处理程序

void DlgMyDocumentTypeSet::InitCombDocumentType()
{
    //m_crtlCombDocumentType.Create(CB_SETEDITSEL,CRect(0,0,5,5),this, IDC_COMBO_DOCUMENT_TYPE);
    m_crtlCombDocumentType.AddString(_T("mdi文件类型"));
    m_crtlCombDocumentType.AddString(_T("我的文件类型1"));

    switch (m_nDocumentType)
    {
    case EFileDocumentType::E_FILE_DOCUMENT_TYPE_MID:
        m_crtlCombDocumentType.SetCurSel(0);
        break;

    case  EFileDocumentType::E_FILE_DOCUMENT_TYPE_MYDOCUMENT1:
        m_crtlCombDocumentType.SetCurSel(1);
        break;
    }

    (CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
}


void DlgMyDocumentTypeSet::OnOK()
{
    int nIndex = m_crtlCombDocumentType.GetCurSel();
    switch (nIndex)
    {
    case 0:
        m_nDocumentType = EFileDocumentType::E_FILE_DOCUMENT_TYPE_MID;
        break;

    case  1:
        m_nDocumentType = EFileDocumentType::E_FILE_DOCUMENT_TYPE_MYDOCUMENT1;
        break;
    }

    CDialogEx::OnOK();
}


void DlgMyDocumentTypeSet::OnCancel()
{
    CDialogEx::OnCancel();
}


BOOL DlgMyDocumentTypeSet::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    InitCombDocumentType();

    return TRUE;  // return TRUE unless you set the focus to a control
    // 异常: OCX 属性页应返回 FALSE
}


void DlgMyDocumentTypeSet::OnBnClickedOk()
{
    // TODO: 在此添加控件通知处理程序代码
    CDialogEx::OnOK();
}


void DlgMyDocumentTypeSet::OnPaint()
{
    CPaintDC dc(this);

    CRect rect;
    GetClientRect(rect);
    dc.FillSolidRect(rect, RGB(200, 230, 255));  //设置背景颜色
}


HBRUSH DlgMyDocumentTypeSet::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
    HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

    if(pWnd->GetDlgCtrlID() == IDOK)    //主界面
    {
        //pDC->SetBkMode(TRANSPARENT);
        pDC->SetBkColor(RGB(100, 0, 0));  //字体背景色
        return HBRUSH(GetStockObject(HOLLOW_BRUSH));
    }
    return hbr;
}
