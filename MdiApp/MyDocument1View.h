#pragma once
#include <afxwin.h>
#include "MyDocument1.h"

class MyDocument1View:public CView
{
protected: // 仅从序列化创建
    MyDocument1View() noexcept;
    DECLARE_DYNCREATE(MyDocument1View)

        // 特性
public:
    MyDocument1 *GetDocument() const;

    // 操作
public:

    // 重写
public:
    virtual void OnDraw(CDC *pDC);  // 重写以绘制该视图
    virtual BOOL PreCreateWindow(CREATESTRUCT &cs);
protected:
    virtual BOOL OnPreparePrinting(CPrintInfo *pInfo);
    virtual void OnBeginPrinting(CDC *pDC, CPrintInfo *pInfo);
    virtual void OnEndPrinting(CDC *pDC, CPrintInfo *pInfo);

    // 实现
public:
    virtual ~MyDocument1View();
#ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext &dc) const;
#endif

protected:

    // 生成的消息映射函数
protected:
    afx_msg void OnFilePrintPreview();
    afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
    afx_msg void OnContextMenu(CWnd *pWnd, CPoint point);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    DECLARE_MESSAGE_MAP()



        bool	m_LButtonDown = false;
    CPoint  m_PointStart;
    CPoint  m_PointEnd;


};



#ifndef _DEBUG  // MdiAppView.cpp 中的调试版本
inline MyDocument1 *MyDocument1View::GetDocument() const
{
    return reinterpret_cast<MyDocument1 *>(m_pDocument);
}
#endif