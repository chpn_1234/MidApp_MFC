﻿// 这段 MFC 示例源代码演示如何使用 MFC Microsoft Office Fluent 用户界面
// (“Fluent UI”)。该示例仅供参考，
// 用以补充《Microsoft 基础类参考》和
// MFC C++ 库软件随附的相关电子文档。
// 复制、使用或分发 Fluent UI 的许可条款是单独提供的。
// 若要了解有关 Fluent UI 许可计划的详细信息，请访问
// https://go.microsoft.com/fwlink/?LinkId=238214.
//
// 版权所有(C) Microsoft Corporation
// 保留所有权利。

// MdiApp.cpp: 定义应用程序的类行为。
//

#include "pch.h"
#include "framework.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "MdiApp.h"
#include "MainFrm.h"

#include "ChildFrm.h"
#include "MdiAppDoc.h"
#include "MdiAppView.h"

#include "MyDocument1.h"
#include "MyDocument1View.h"

#include "DlgMyDocumentTypeSet.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// MdiAppApp
EFileDocumentType MdiAppApp::m_FileDocumentType = EFileDocumentType::E_FILE_DOCUMENT_TYPE_MID;

BEGIN_MESSAGE_MAP(MdiAppApp, CWinAppEx)
    ON_COMMAND(ID_APP_ABOUT, &MdiAppApp::OnAppAbout)
    // 基于文件的标准文档命令
    //ON_COMMAND(ID_FILE_NEW, &CWinAppEx::OnFileNew)
    ON_COMMAND(ID_FILE_NEW, &MdiAppApp::onMyFileNew)

    ON_COMMAND(ID_FILE_OPEN, &CWinAppEx::OnFileOpen)
    ////ON_COMMAND(ID_FILE_OPEN, &MdiAppApp::onMyFileOpen)

    //ON_COMMAND(ID_FILE_CLOSE, &MdiAppApp::onCloseMyDocument)


    // 标准打印设置命令
    ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinAppEx::OnFilePrintSetup)
    ON_COMMAND(ID_MDI_DOCUMENT_TYPE, &MdiAppApp::onMdiDocumentType)
    ON_COMMAND(ID_MY_DOCUMENT_TYP1, &MdiAppApp::onMyDocumentTyp1)
END_MESSAGE_MAP()


// MdiAppApp 构造

MdiAppApp::MdiAppApp() noexcept
{
    m_bHiColorIcons = TRUE;


    m_nAppLook = 0;
    // 支持重新启动管理器
    m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_NO_AUTOSAVE;
#ifdef _MANAGED
    // 如果应用程序是利用公共语言运行时支持(/clr)构建的，则: 
    //     1) 必须有此附加设置，“重新启动管理器”支持才能正常工作。
    //     2) 在您的项目中，您必须按照生成顺序向 System.Windows.Forms 添加引用。
    System::Windows::Forms::Application::SetUnhandledExceptionMode(System::Windows::Forms::UnhandledExceptionMode::ThrowException);
#endif

    // TODO: 将以下应用程序 ID 字符串替换为唯一的 ID 字符串；建议的字符串格式
    //为 CompanyName.ProductName.SubProduct.VersionInformation
    SetAppID(_T("MdiApp.AppID.NoVersion"));

    // TODO:  在此处添加构造代码，
    // 将所有重要的初始化放置在 InitInstance 中
}

// 唯一的 MdiAppApp 对象

MdiAppApp theApp;


// MdiAppApp 初始化

BOOL MdiAppApp::InitInstance()
{
    // 如果一个运行在 Windows XP 上的应用程序清单指定要
    // 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
    //则需要 InitCommonControlsEx()。  否则，将无法创建窗口。
    INITCOMMONCONTROLSEX InitCtrls;
    InitCtrls.dwSize = sizeof(InitCtrls);
    // 将它设置为包括所有要在应用程序中使用的
    // 公共控件类。
    InitCtrls.dwICC = ICC_WIN95_CLASSES;
    InitCommonControlsEx(&InitCtrls);

    CWinAppEx::InitInstance();


    // 初始化 OLE 库
    if (!AfxOleInit())
    {
        AfxMessageBox(IDP_OLE_INIT_FAILED);
        return FALSE;
    }

    AfxEnableControlContainer();

    EnableTaskbarInteraction();

    // 使用 RichEdit 控件需要 AfxInitRichEdit2()
    // AfxInitRichEdit2();

    // 标准初始化
    // 如果未使用这些功能并希望减小
    // 最终可执行文件的大小，则应移除下列
    // 不需要的特定初始化例程
    // 更改用于存储设置的注册表项
    // TODO: 应适当修改该字符串，
    // 例如修改为公司或组织名
    SetRegistryKey(_T("应用程序向导生成的本地应用程序"));
    LoadStdProfileSettings(4);  // 加载标准 INI 文件选项(包括 MRU)


    InitContextMenuManager();
    InitShellManager();

    InitKeyboardManager();

    InitTooltipManager();
    CMFCToolTipInfo ttParams;
    ttParams.m_bVislManagerTheme = TRUE;
    theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL,
                                                 RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);





    // 注册应用程序的文档模板。  文档模板
    // 将用作文档、框架窗口和视图之间的连接
    m_pMdiDocumentType = new CMultiDocTemplate(IDR_MDI_DOCUMENT_TYPE,
                                               RUNTIME_CLASS(CMdiAppDoc),
                                               RUNTIME_CLASS(CChildFrame), // 自定义 MDI 子框架
                                               RUNTIME_CLASS(CMdiAppView));


    if (!m_pMdiDocumentType)
    {
        return FALSE;
    }
    else
    {
        AddDocTemplate(m_pMdiDocumentType);
    }



    m_pMyDocumentType1 = new CMultiDocTemplate(IDR_MY_DOCUMENT_TYPE1,
                                               RUNTIME_CLASS(MyDocument1),
                                               RUNTIME_CLASS(CChildFrame), // 自定义 MDI 子框架
                                               RUNTIME_CLASS(MyDocument1View));


    if (!m_pMyDocumentType1)
    {
        return FALSE;
    }
    else
    {
        AddDocTemplate(m_pMyDocumentType1);
    }


    m_pAppDocumentManager = new CDocManager();
    m_pAppDocumentManager->AddDocTemplate(m_pMdiDocumentType);
    m_pAppDocumentManager->AddDocTemplate(m_pMyDocumentType1);


    // 创建主 MDI 框架窗口
    CMainFrame *pMainFrame = new CMainFrame;
    if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
    {
        delete pMainFrame;
        return FALSE;
    }
    m_pMainWnd = pMainFrame;


    // 分析标准 shell 命令、DDE、打开文件操作的命令行
    CCommandLineInfo cmdInfo;
    ParseCommandLine(cmdInfo);


    // 调度在命令行中指定的命令。  如果
    // 用 /RegServer、/Register、/Unregserver 或 /Unregister 启动应用程序，则返回 FALSE。
    if (!ProcessShellCommand(cmdInfo))
    {
        return FALSE;
    }


    // 主窗口已初始化，因此显示它并对其进行更新
    pMainFrame->ShowWindow(m_nCmdShow);
    pMainFrame->UpdateWindow();


    return TRUE;
}


int MdiAppApp::ExitInstance()
{
    //TODO: 处理可能已添加的附加资源
    AfxOleTerm(FALSE);

    return CWinAppEx::ExitInstance();
}


// MdiAppApp 消息处理程序

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
    CAboutDlg() noexcept;

    // 对话框数据
#ifdef AFX_DESIGN_TIME
    enum
    {
        IDD = IDD_ABOUTBOX
    };
#endif

protected:
    virtual void DoDataExchange(CDataExchange *pDX);    // DDX/DDV 支持

    // 实现
protected:
    DECLARE_MESSAGE_MAP()
};


CAboutDlg::CAboutDlg() noexcept : CDialogEx(IDD_ABOUTBOX)
{
}


void CAboutDlg::DoDataExchange(CDataExchange *pDX)
{
    CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// 用于运行对话框的应用程序命令
void MdiAppApp::OnAppAbout()
{
    CAboutDlg aboutDlg;
    aboutDlg.DoModal();
}


// MdiAppApp 自定义加载/保存方法

void MdiAppApp::PreLoadState()
{
    BOOL bNameValid;
    CString strName;
    bNameValid = strName.LoadString(IDS_EDIT_MENU);
    ASSERT(bNameValid);
    GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);
    bNameValid = strName.LoadString(IDS_EXPLORER);
    ASSERT(bNameValid);
    GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EXPLORER);
}


void MdiAppApp::LoadCustomState()
{
}


void MdiAppApp::SaveCustomState()
{
}


// MdiAppApp 消息处理程序

void MdiAppApp::onMyFileNew()
{
    DlgMyDocumentTypeSet dlg;
    dlg.DoModal();
    m_FileDocumentType = dlg.GetDocumentType();


    switch (m_FileDocumentType)
    {
    case EFileDocumentType::E_FILE_DOCUMENT_TYPE_MID:
        onMdiDocumentType();
        break;

    case EFileDocumentType::E_FILE_DOCUMENT_TYPE_MYDOCUMENT1:
        onMyDocumentTyp1();
        break;
    }

    ((CMainFrame *)AfxGetMainWnd())->updateMyFileView();
}


void MdiAppApp::onMdiDocumentType()
{
    ASSERT_KINDOF(CMultiDocTemplate, m_pMdiDocumentType);
    m_pMdiDocumentType->OpenDocumentFile(NULL);

}


void MdiAppApp::onMyDocumentTyp1()
{
    ASSERT_KINDOF(CMultiDocTemplate, m_pMyDocumentType1);
    m_pMyDocumentType1->OpenDocumentFile(NULL);
}


CDocManager *MdiAppApp::getAppDocumentManager()
{
    // TODO: 在此处添加实现代码.
    return m_pAppDocumentManager;
}


CDocument *MdiAppApp::OpenDocumentFile(LPCTSTR lpszFileName)
{
    // TODO: 在此添加专用代码和/或调用基类
    CDocument *pOpenedDocumentFile = CWinAppEx::OpenDocumentFile(lpszFileName);

    ((CMainFrame *)AfxGetMainWnd())->updateMyFileView();

    return pOpenedDocumentFile;
}


void MdiAppApp::onCloseMyDocument()
{
    ((CMainFrame *)AfxGetMainWnd())->updateMyFileView();
}


