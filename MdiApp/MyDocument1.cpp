#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "MdiApp.h"
#endif
#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "MyDocument1.h"
#include "MainFrm.h"
// MyDocument1


IMPLEMENT_DYNCREATE(MyDocument1, CDocument)
BEGIN_MESSAGE_MAP(MyDocument1, CDocument)
END_MESSAGE_MAP()


// MyDocument1 构造/析构

MyDocument1::MyDocument1() noexcept
{
    // TODO: 在此添加一次性构造代码

}



MyDocument1::~MyDocument1()
{
}



BOOL MyDocument1::OnNewDocument()
{
    if (!CDocument::OnNewDocument())
        return FALSE;

    // TODO: 在此添加重新初始化代码
    // (SDI 文档将重用该文档)

    //((CMainFrame *)AfxGetMainWnd())->UpdateMyFileView();

    
    return TRUE;
}




// CMdiAppDoc 序列化

void MyDocument1::Serialize(CArchive &ar)
{
    if (ar.IsStoring())
    {
        // TODO: add storing code here
        int n = (int)m_typedPtrList.GetCount();//+++++++++++++++++++++得到数目
        ar << n;//++++++++++++++++++++++++++++++++++++++串行化输入总数目
        POSITION pos = m_typedPtrList.GetHeadPosition();
        for (int i = 0; i < n; i++)
        {
            CRetangle *retangle = m_typedPtrList.GetNext(pos);//+++++++得到对象链表
            retangle->Serialize(ar);//+++++++++++++++++++++串行化输入
        }
    }
    else
    {
        // TODO: add loading code here
        int n;
        ar >> n;//++++++++++++++++++++++++++++++++++++得到文件中对象目数
        while (m_typedPtrList.IsEmpty() == FALSE)
        {
            delete m_typedPtrList.GetHead();
            m_typedPtrList.RemoveHead();
        }


        POSITION pos = m_typedPtrList.GetHeadPosition();          //得到当前头
        for (int i = 0; i < n; i++)
        {
            CRetangle *retangle = new CRetangle();
            retangle->Serialize(ar);							//串行化输出
            m_typedPtrList.AddTail(retangle);
        }
    }
}



#ifdef SHARED_HANDLERS

// 缩略图的支持
void CMdiAppDoc::OnDrawThumbnail(CDC &dc, LPRECT lprcBounds)
{
    // 修改此代码以绘制文档数据
    dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

    CString strText = _T("TODO: implement thumbnail drawing here");
    LOGFONT lf;

    CFont *pDefaultGUIFont = CFont::FromHandle((HFONT)GetStockObject(DEFAULT_GUI_FONT));
    pDefaultGUIFont->GetLogFont(&lf);
    lf.lfHeight = 36;

    CFont fontDraw;
    fontDraw.CreateFontIndirect(&lf);

    CFont *pOldFont = dc.SelectObject(&fontDraw);
    dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
    dc.SelectObject(pOldFont);
}



// 搜索处理程序的支持
void CMdiAppDoc::InitializeSearchContent()
{
    CString strSearchContent;
    // 从文档数据设置搜索内容。
    // 内容部分应由“;”分隔

    // 例如:     strSearchContent = _T("point;rectangle;circle;ole object;")；
    SetSearchContent(strSearchContent);
}



void CMdiAppDoc::SetSearchContent(const CString &value)
{
    if (value.IsEmpty())
    {
        RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
    }
    else
    {
        CMFCFilterChunkValueImpl *pChunk = nullptr;
        ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
        if (pChunk != nullptr)
        {
            pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
            SetChunkValue(pChunk);
        }
    }
}

#endif // SHARED_HANDLERS



// CMdiAppDoc 诊断

#ifdef _DEBUG
void MyDocument1::AssertValid() const
{
    CDocument::AssertValid();
}




void MyDocument1::Dump(CDumpContext &dc) const
{
    CDocument::Dump(dc);
}
#endif //_DEBUG


// CMdiAppDoc 命令






//------CRetangle------
CRetangle::CRetangle(CPoint a, CPoint b)//++++++++++++++++++++++
{
    PointX = a;//+++++++++++++++++++++++++++
    PointY = b;//+++++++++++++++++++++++++++++
}



CRetangle::~CRetangle()
{
}



void CRetangle::Serialize(CArchive &ar)
{
    if (ar.IsStoring())
    {//storing code

        ar << '[' << PointX.x << ',' << PointX.y << ']' << '[' << PointY.x << ',' << PointY.y << ']';//变量输入到文件
    }
    else
    {//loading code
        char Temp;
        ar >> Temp >> PointX.x >> Temp >> PointX.y >> Temp >> Temp >> PointY.x >> Temp >> PointY.y >> Temp;//从文件读取变量
    }
}


void MyDocument1::OnCloseDocument()
{
    // TODO: 在此添加专用代码和/或调用基类
  
    CDocument::OnCloseDocument();
    ((CMainFrame*)AfxGetMainWnd())->updateMyFileView();
}


BOOL MyDocument1::OnSaveDocument(LPCTSTR lpszPathName)
{
    // TODO: 在此添加专用代码和/或调用基类
  
    auto saveRetrun = CDocument::OnSaveDocument(lpszPathName);
    return   saveRetrun;
}



BOOL MyDocument1::OnOpenDocument(LPCTSTR lpszPathName)
{
    if (!CDocument::OnOpenDocument(lpszPathName)) 
    {
        return FALSE;
    }
    
    return TRUE;
}
